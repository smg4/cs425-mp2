package main

import (
	"os"
	"log"
    "fmt"
	"net"
    "sync"
    "sort"
    "time"
    "strconv"
)

const (
    ALIVE = "0"
    NOT = "1"
	JOIN = 0
	LEAVE = 1
	TIMEOUT = 2
)

var inGroup bool
var m = &sync.Mutex{}
var quitEvents chan int
var quitHeartbeats chan int
//var firstOpen bool = true
var successor1 memberEntry
var successor2 memberEntry
var successor3 memberEntry
var membershipList []memberEntry
var introducer string = "fa19-cs425-g45-01.cs.illinois.edu"

type memberEntry struct {
    hostname string
    status string
    lastUpdate string
}

func checkError(err error) {
    if err != nil {
        log.Fatal("Fatal error: ", err)
    }
}

func findTimeLength(str string, i int) int {
    count := 0
    for j := i + len(introducer) + 1; j < len(str) && string(str[j]) != "$"; j++ {
        count++
    }

    return count
}

func contactIntroducer() bool {
    server, err := net.ResolveUDPAddr("udp", introducer + ":5600")
    checkError(err)

    conn, err := net.DialUDP("udp", nil, server)
    checkError(err)

    hostname, _ := os.Hostname()
    _, err = conn.Write([]byte("JOIN" + hostname))
    if err != nil {
        fmt.Printf("Could not contact introducer!\n")
        return false
    }

    var buf [1024]byte
    n, err := conn.Read(buf[0:])
    if err != nil {
        fmt.Printf("Could not contact introducer!\n")
        return false
    }

    conn.Close()
    membershipListStr := string(buf[0:n])
    for i := 0; i < len(membershipListStr); i += (len(introducer)) {
        res := findTimeLength(membershipListStr, i)
        entry := memberEntry {
            hostname: string(membershipListStr[i:(i+len(introducer))]),
            status: string(membershipListStr[i+len(introducer)]),
            lastUpdate: string(membershipListStr[i+len(introducer)+1:i+len(introducer)+1+res]),
        }
        membershipList = append(membershipList, entry)
        i += (2 + res)
    }
    sort.Slice(membershipList, func(i, j int) bool {
        a, _ := strconv.Atoi(membershipList[i].hostname[15:17])
        b, _ := strconv.Atoi(membershipList[j].hostname[15:17])
        return a < b
    })

    return true
}

func machineNum(host string) int {
    a, _ := strconv.Atoi(host[15:17])
    return a
}

func changeMemListStatus(hostname string, stat string) {
    for i, elem := range membershipList {
        if elem.hostname == hostname {
            entry := memberEntry {
                hostname: hostname,
                status: stat,
                lastUpdate: elem.lastUpdate,
            }
            membershipList[i] = entry
            break
        }
    }
}

func updateHeartbeat() {
    m.Lock()
    sort.Slice(membershipList, func(i, j int) bool {
        a, _ := strconv.Atoi(membershipList[i].hostname[15:17])
        b, _ := strconv.Atoi(membershipList[j].hostname[15:17])
        return a < b
    })
    start := 0
    hostname, _ := os.Hostname()
    for ; start < len(membershipList); start++ {
        if membershipList[start].hostname == hostname {
            break
        }
    }
    successor1, successor2, successor3 = memberEntry{}, memberEntry{}, memberEntry{}

    count := 0
    n := len(membershipList)
    i := (start + 1) % n
    for ; count < n; i++ {
        e := membershipList[i % n]
        if e.status == ALIVE && e.hostname != hostname {
            successor1 = e
            break
        }
        count++
    }

    count = 0
    j := (i + 1) % n
    for ; count < n; j++ {
        e := membershipList[j % n]
        if e.status == ALIVE && e.hostname != hostname && e.hostname != successor1.hostname {
            successor2 = e
            break
        }
        count++
    }

    count = 0
    k := (j + 1) % n
    for ; count < n; k++ {
        e := membershipList[k % n]
        if e.status == ALIVE && e.hostname != hostname && e.hostname != successor1.hostname && e.hostname != successor2.hostname {
            successor3 = e
            break
        }
        count++
    }
    m.Unlock()
}

func mergeMemLists(hosts string) {
    m.Lock()
    var other []memberEntry
    for i := 0; i < len(hosts); i += (len(introducer)) {
        res := findTimeLength(hosts, i)
        entry := memberEntry {
            hostname: string(hosts[i:(i+len(introducer))]),
            status: string(hosts[i+len(introducer)]),
            lastUpdate: string(hosts[i+len(introducer)+1:i+len(introducer)+1+res]),
        }
        other = append(other, entry)
        i += (2 + res)
    }

	thisHost, _ := os.Hostname()
    newMemList := make([]memberEntry, 0)

    i, j := 0, 0
    for ; i < len(membershipList) && j < len(other); {
		var otherTime time.Time
		var memberTime time.Time
		a := machineNum(membershipList[i].hostname)
        b := machineNum(other[j].hostname)
		otherTime.UnmarshalText([]byte(other[j].lastUpdate))
		memberTime.UnmarshalText([]byte(membershipList[i].lastUpdate))

		cont := false
		if time.Now().Sub(memberTime).Seconds() > float64(3.5) {
			i++
			cont = true
		}
		if time.Now().Sub(otherTime).Seconds() > float64(3.5) {
			j++
			cont = true
		}
		if cont {
			continue
		}

        if a == b {
            if other[j].status != membershipList[i].status {
				if other[j].status != NOT || other[j].hostname != thisHost {
					membershipList[i].status = other[j].status
				}
            }
            if otherTime.Sub(memberTime) > 0 && other[j].status == ALIVE {
                membershipList[i].lastUpdate = other[j].lastUpdate
            }
            newMemList = append(newMemList, memberEntry(membershipList[i]))
            i++
            j++
        } else if a < b {
            newMemList = append(newMemList, memberEntry(membershipList[i]))
            i++
        } else {
            newMemList = append(newMemList, memberEntry(other[j]))
            j++
        }
    }

    for ; i < len(membershipList); i++ {
        newMemList = append(newMemList, memberEntry(membershipList[i]))
    }

    for ; j < len(other); j++ {
        newMemList = append(newMemList, memberEntry(other[j]))
    }

    membershipList = newMemList
    m.Unlock()
    updateHeartbeat()
}

func sendBackList(conn *net.UDPConn, addr *net.UDPAddr) {
    msg := ""
    for _, v := range membershipList {
        msg += v.hostname
        msg += v.status
        msg += v.lastUpdate
        msg += "$"
    }
    conn.WriteToUDP([]byte(msg), addr)
}

func handleEvents(conn *net.UDPConn) {
    var buf [1024]byte
    n, addr, err := conn.ReadFromUDP(buf[0:])
    if err != nil {
        return
    }

    msg := string(buf[0:n])
    if msg[0:4] == "JOIN" {
        m.Lock()
        found := false
        for i, elem := range membershipList {
            if elem.hostname == msg[4:] {
                bytes, _ := time.Now().MarshalText()
                entry := memberEntry {
                    hostname: elem.hostname,
                    status: ALIVE,
                    lastUpdate: string(bytes),
                }
                membershipList[i] = entry
                found = true
                break
            }
        }

        if !found {
            bytes, _ := time.Now().MarshalText()
            entry := memberEntry {
                hostname: string(msg[4:]),
                status: ALIVE,
                lastUpdate: string(bytes),
            }
            membershipList = append(membershipList, entry)
        }
        m.Unlock()
        updateHeartbeat()
        m.Lock()
        sort.Slice(membershipList, func(i, j int) bool {
            a, _ := strconv.Atoi(membershipList[i].hostname[15:17])
            b, _ := strconv.Atoi(membershipList[j].hostname[15:17])
            return a < b
        })
        sendBackList(conn, addr)
        m.Unlock()
    } else if msg[0:4] == "BEAT" {
        conn.WriteToUDP([]byte("OK"), addr)
        mergeMemLists(string(msg[4:]))
    } else if msg[0:4] == "LEAV" {
        conn.WriteToUDP([]byte("OK"), addr)
        changeMemListStatus(string(msg[4:]), NOT)
        updateHeartbeat()
    }
}

func eventListener(quit chan int) {
    server, err := net.ResolveUDPAddr("udp", ":5600")
    checkError(err)

    serverConn, err := net.ListenUDP("udp", server)
    checkError(err)

    defer serverConn.Close()

    for {
        select {
        case <-quit:
            serverConn.Close()
            return
        default:
            handleEvents(serverConn)
        }
    }
}

func sendBeat(entry memberEntry) {
    if entry.hostname == "" {
        return
    }

    server, err := net.ResolveUDPAddr("udp", entry.hostname + ":5600")
    checkError(err)

    conn, err := net.DialUDP("udp", nil, server)
    checkError(err)

    defer conn.Close()
    m.Lock()
    msg := "BEAT"
    for _, v := range membershipList {
        msg += v.hostname
        msg += v.status
        msg += v.lastUpdate
        msg += "$"
    }
    m.Unlock()
    _, err = conn.Write([]byte(msg))
    checkError(err)

    m.Lock()
    var buf [1024]byte
    n, _ := conn.Read(buf[0:])
    if string(buf[0:n]) == "OK" {
        for i, elem := range membershipList {
            if elem.hostname == entry.hostname {
                bytes, _ := time.Now().MarshalText()
                newEntry := memberEntry {
                    hostname: entry.hostname,
                    status: ALIVE,
                    lastUpdate: string(bytes),
                }
                membershipList[i] = newEntry
                break
            }
        }
    }
    m.Unlock()
}

func generateLogs(hostname string, action int) {
	host, _ := os.Hostname()
	mnum := machineNum(host)
	var logFile *os.File
	var err error
	//if firstOpen {
	//	logFile, err = os.OpenFile("machine." + strconv.Itoa(mnum) + ".log", os.O_WRONLY | os.O_TRUNC | os.O_APPEND, 0666)
	//	firstOpen = false
	//} else {
		logFile, err = os.OpenFile("machine." + strconv.Itoa(mnum) + ".log", os.O_WRONLY | os.O_APPEND, 0666)
	//}
	if err != nil {
		return
	}
	defer logFile.Close()

	switch action {
	case JOIN:
		logFile.Write([]byte("JOIN: Machine " + strconv.Itoa(mnum) + " at " + time.Now().String() + "\n"))
	case LEAVE: // LEAVE
		logFile.Write([]byte("LEAVE: Machine " + strconv.Itoa(mnum) + " at " + time.Now().String() + "\n"))
	case TIMEOUT: // TIMEOUT
		logFile.Write([]byte("TIMEOUT DETECTED: Machine " + strconv.Itoa(mnum) + " at " + time.Now().String() + "\n"))
	}
	logFile.Sync()
}

func heartbeat(quit chan int) {
	pingIntroducer := 0
    for {
        select {
        case <-quit:
            return
        default:
			pingIntroducer++
            sendBeat(successor1)
            sendBeat(successor2)
            sendBeat(successor3)

			if pingIntroducer % 3 == 0 {
				pingIntroducer = 0
				intro := memberEntry {
					hostname: introducer,
					status: "0",
					lastUpdate: "0",
				}
				sendBeat(intro)
			}

			m.Lock()
			host, _ := os.Hostname()
            if successor1.hostname != "" {
               var start time.Time
               start.UnmarshalText([]byte(successor1.lastUpdate))

               if time.Now().Sub(start).Seconds() > float64(3.5) {
				   if host != successor1.hostname {
					   changeMemListStatus(successor1.hostname, NOT)
					   generateLogs(successor1.hostname, TIMEOUT)
				   }
               }
            }
            if successor2.hostname != "" {
               var start time.Time
               start.UnmarshalText([]byte(successor2.lastUpdate))

               if time.Now().Sub(start).Seconds() > float64(3.5) {
				   if host != successor2.hostname {
					   changeMemListStatus(successor2.hostname, NOT)
					   generateLogs(successor2.hostname, TIMEOUT)
				   }
               }
            }
            if successor3.hostname != "" {
               var start time.Time
               start.UnmarshalText([]byte(successor3.lastUpdate))

               if time.Now().Sub(start).Seconds() > float64(3.5) {
				   if host != successor3.hostname {
					   changeMemListStatus(successor3.hostname, NOT)
					   generateLogs(successor3.hostname, TIMEOUT)
				   }
               }
            }
			m.Unlock()

            updateHeartbeat()
            time.Sleep(2 * time.Second)
        }
    }
}

func main() {
    hostname, _ := os.Hostname()
    inGroup = false

    for {
        var line string
        fmt.Scanln(&line)

        if line == "leave" && inGroup {
            inGroup = false
			generateLogs(hostname, LEAVE)
            if successor1.hostname != "" {
                server, _ := net.ResolveUDPAddr("udp", successor1.hostname + ":5600")
                conn, _ := net.DialUDP("udp", nil, server)
                conn.Write([]byte("LEAV" + hostname))
                var buf [1024]byte
                conn.Read(buf[0:])
                conn.Close()
            }
            if successor2.hostname != "" {
                server, _ := net.ResolveUDPAddr("udp", successor2.hostname + ":5600")
                conn, _ := net.DialUDP("udp", nil, server)
                conn.Write([]byte("LEAV" + hostname))
                var buf [1024]byte
                conn.Read(buf[0:])
                conn.Close()
            }
            if successor3.hostname != "" {
                server, _ := net.ResolveUDPAddr("udp", successor3.hostname + ":5600")
                conn, _ := net.DialUDP("udp", nil, server)
                conn.Write([]byte("LEAV" + hostname))
                var buf [1024]byte
                conn.Read(buf[0:])
                conn.Close()
            }

			fmt.Printf("LEFT RING\n")
            close(quitEvents)
            close(quitHeartbeats)
        } else if line == "memlist" && inGroup {
            fmt.Printf("Membership List:\n")
            m.Lock()
            for _, elem := range membershipList {
                if elem.status == ALIVE {
                    fmt.Printf("%v\n", elem)
                }
            }
            m.Unlock()
        } else if line == "id" && inGroup {
            m.Lock()
            for _, entry := range membershipList {
                if entry.hostname == hostname {
                    fmt.Printf("Self ID: %s%s\n", entry.hostname, entry.lastUpdate)
                    break
                }
            }
            m.Unlock()
        } else if line == "join" && !inGroup {
            joined := true
			generateLogs(hostname, JOIN)
            if hostname != introducer {
                joined = contactIntroducer()
            } else {
                start := time.Now()
                bytes, _ := start.MarshalText()
                entry := memberEntry {
                    hostname: introducer,
                    status: ALIVE,
                    lastUpdate: string(bytes),
                }
                membershipList = append(membershipList, entry)
                sort.Slice(membershipList, func(i, j int) bool {
                    a, _ := strconv.Atoi(membershipList[i].hostname[15:17])
                    b, _ := strconv.Atoi(membershipList[j].hostname[15:17])
                    return a < b
                })
            }

            if joined {
				fmt.Printf("JOINED RING\n")
                updateHeartbeat()
                inGroup = true

                quitEvents = make(chan int)
                quitHeartbeats = make(chan int)
                go eventListener(quitEvents)
                go heartbeat(quitHeartbeats)
            }
        } else {
            fmt.Printf("--------Invalid Command--------\nList of Valid Commands:\n")
            fmt.Printf("\t- %s\n\t- %s\n\t- %s\n\t- %s\n", "join", "leave (in ring ONLY)", "memlist (in ring ONLY)", "id (in ring ONLY)")
        }
    }
}
