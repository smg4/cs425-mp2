# Directory Structure
Because this project is written in go, you must ensure that you have a correct directory structure to run it. In your home directory, create a directory named ```go```. Inside that directory, create two more directories, one named ```src``` and one named ```bin```. Then clone this repository into the ```src``` directory. Do this for each machine you plan to use.

# Building and Running Project
First run ```go build node.go``` on the command line of each machine. Then on every machine, run ```./node```. To have a daemon process that is running join the extended ring, simply write ```join``` to the terminal of that machine. A full list of commands you can use at each machine is listed below:
1. ```join``` --> join the ring
2. ```leave``` --> leave the ring
3. ```memlist``` --> show membership list
4. ```id``` --> show node id

Play around with what you can do!